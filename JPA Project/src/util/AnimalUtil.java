package util;

import java.util.List;

import java.util.Scanner;

import model.Animal;

public class AnimalUtil {
	
	static DatabaseUtil dbUtil =new DatabaseUtil();
	
	public static void createAnimal() throws Exception {
		String nume = null;
		String specie = null;
		int id;
		//Reading animal's name and id from keyboard
		Scanner scanner=new Scanner(System.in);
		System.out.println("Insert animal name");
			nume = scanner.nextLine();
		System.out.println("Insert animal species");
		    specie = scanner.nextLine();
		System.out.println("Insert animal id");
			id = Integer.parseInt(scanner.nextLine());
		
		//throws exception if key is already taken
		//if(idCheck(id)) {
		//	new Exception("The id is already taken, try again ");  //TO DO: do something to show exception message
		//}
			
		
		//inserting creating the animal
		Animal myAnimal = new Animal();
		myAnimal.setIdAnimal(id);
		myAnimal.setNume(nume);
		myAnimal.setSpecie(specie);
		
		//pushing my animal to DB
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.saveAnimal(myAnimal);
		dbUtil.commitTransaction();
		dbUtil.printAllAnimalsFromDB();
		dbUtil.closeEntityManager();
	}
	
	
	

	
	//Method for updating an animal after looking by id
	public static void updateAnimal() throws Exception {
		
		System.out.println("Update the animal with id: ");
		Scanner scanner = new Scanner(System.in);
		int lookForThis = Integer.parseInt(scanner.nextLine());
		
		System.out.println("The new name for the animal is :");
		String newName = scanner.nextLine();
		
		System.out.println("The new species for the animal is:");
		String newSpecies = scanner.nextLine();
		
		dbUtil.setUp();
		dbUtil.startTransaction();
		Animal myAnimal = dbUtil.findAnimal(lookForThis);
		myAnimal.setNume(newName);
		myAnimal.setSpecie(newSpecies);
		dbUtil.commitTransaction();
		dbUtil.printAllAnimalsFromDB();
		dbUtil.closeEntityManager();
		

	}
	

	//method for reading all animals
	public static void PrintAllAnimals( ) throws Exception{

		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.printAllAnimalsFromDB();
		dbUtil.closeEntityManager();
	}
public static void deleteAnimal() throws Exception {
		
		System.out.println("Delete the animal with id: ");
		Scanner scanner = new Scanner(System.in);
		int lookForThis = Integer.parseInt(scanner.nextLine());
		
		dbUtil.setUp();
		dbUtil.startTransaction();
		Animal myAnimal = dbUtil.findAnimal(lookForThis);
		dbUtil.entityManager.remove(myAnimal);
		dbUtil.commitTransaction();
		dbUtil.printAllAnimalsFromDB();
		dbUtil.closeEntityManager();
		

	}

public static Animal findAnimal(int lookForThis) throws Exception {
	dbUtil.setUp();
	dbUtil.startTransaction();
	Animal myAnimal = dbUtil.findAnimal(lookForThis);
	dbUtil.closeEntityManager();
	
	return myAnimal;
}

public static Animal findAnimal() throws Exception {
	Scanner scanner = new Scanner(System.in);
	int lookForThis = Integer.parseInt(scanner.nextLine());
	
	dbUtil.setUp();
	dbUtil.startTransaction();
	Animal myAnimal = dbUtil.findAnimal(lookForThis);
	dbUtil.closeEntityManager();
	
	return myAnimal;
	
	}
}
