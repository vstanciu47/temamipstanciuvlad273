package util;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import model.Animal;
import model.Personalmedical;
import model.Programare;


public class DatabaseUtil {
	private static DatabaseUtil instance = new DatabaseUtil();
	public static EntityManagerFactory entityManagerFactory;
	public static DatabaseUtil getInstance() {
		return instance;
	}

	public static void setInstance(DatabaseUtil instance) {
		DatabaseUtil.instance = instance;
	}

	public static EntityManager entityManager;

	public void setUp()  {
		entityManagerFactory = Persistence.createEntityManagerFactory("PetShopJPA");
		entityManager = entityManagerFactory.createEntityManager(); 
		
	}

	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
		
	}
	
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
	
	public void commitTransaction()
	{
		entityManager.getTransaction().commit();
	}
	
	public void closeEntityManager() {
		entityManager.close();
	}
	
	
	public void printAllAnimalsFromDB() {
		List<Animal> results = entityManager.createNativeQuery("Select * from petshop2.animal", Animal.class)
				.getResultList(); 
		for (Animal animal : results) {
			System.out.println("Animal "+ animal.getNume() + " Specie: " + animal.getSpecie() + " ID " + animal.getIdAnimal());
		}
		
	
	}
	
	public Animal findAnimal(int id) {
		Animal myAnimal = entityManager.find(Animal.class, id);
		return myAnimal;
	}

	
	//method for searching personal medical by ID
	public Personalmedical findPersonalMedical(int id) {
		Personalmedical PersonalMedical = entityManager.find(Personalmedical.class, id);
		return PersonalMedical;
	}
	
	public Programare findProgramare(int id) {
		Programare programare = entityManager.find(Programare.class, id);
		return programare;
	}
	
	//prints all Personal Medical
	public void printAllPersonalMedicalFromDB() {
		List<Personalmedical> results = entityManager.createNativeQuery("Select * from petshop2.personalmedical", Personalmedical.class)
				.getResultList();
		for(Personalmedical personalMedical : results) {
			System.out.println("Personal Medical :" + personalMedical.getNume() +" Has specialisation : "+ personalMedical.getSpecializare()+" Has id: " +personalMedical.getIdPersonalMedical());
		}
	}
	
	//pushing programare object to database
	public void saveProgramare(Programare programare) {
		entityManager.persist(programare);
	}
	
	public void printAllProgramareFromDB() {
		List<Programare> results = entityManager.createNativeQuery("Select * from petshop2.programare", Programare.class)
				.getResultList();
		for(Programare programare : results) {
			System.out.println("Programare id: " + programare.getIdProgramare() + " Id Animal " + programare.getIdAnimal() 
			+ "Id Personal Medical:"+programare.getIdPersonalMedical() + "data" + programare.getData()) ;
		}
	}
	
	//pushing PersonalMedical object to database
	public void savePersonalMedical(Personalmedical personalMedical) {
		entityManager.persist(personalMedical);
	}
	
	//returns a list of all animals
	public List<Animal> animalList(){
    List<Animal> animalList = (List<Animal>)entityManager.
    		createQuery("SELECT a FROM Animal a", Animal.class).getResultList();
    return animalList;
	}
	
	//returns a list of all personal medical
	public List<Personalmedical> personalMedicalList(){
		List<Personalmedical> personalMedicalList = (List<Personalmedical>)entityManager.
				createQuery("SELECT p FROM Personalmedical p",Personalmedical.class).getResultList();
		return personalMedicalList;
	}
	
	//returns a list of all programari (rom-gleza?)
	public List<Programare> programareList(){
		List<Programare> programareList = (List<Programare>)entityManager.
				createQuery("SELECT p FROM Programare p",Programare.class).getResultList();
		return programareList;
	}
}
