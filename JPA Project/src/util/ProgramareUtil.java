package util;

import java.util.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.io.Serializable;

import org.eclipse.persistence.jpa.jpql.parser.DateTime;


import model.Personalmedical;
import model.Programare;

public class ProgramareUtil {
	static DatabaseUtil dbUtil =new DatabaseUtil();
	
	public static void createProgramare() throws Exception {	  
		System.out.println("Inserati data si ora programarii : ");
		Scanner scanner=new Scanner(System.in);


		//Reading programare's info   
        System.out.println("Insert programare id");
        	int idProgramare = Integer.parseInt(scanner.nextLine());
		System.out.println("Insert personal medical id");
			int idPersonalMedical = Integer.parseInt(scanner.nextLine());
		System.out.println("Insert animal id");
			int idAnimal = Integer.parseInt(scanner.nextLine());
		System.out.println("Insert Date and Time");
		String s = new String();

		s = scanner.nextLine();
	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MM yyyy HH:mm");
    LocalDateTime date = LocalDateTime.parse(s, formatter);
		Date date2 = Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
	
		//creating Personal medical
		Programare myProgramare = new Programare();
		myProgramare.setIdProgramare(idProgramare);
		myProgramare.setIdAnimal(idAnimal);
		myProgramare.setIdPersonalMedical(idPersonalMedical);
		myProgramare.setData(date2);
		
		
	
		
		//pushing my progrramare to DB
				
				//pushing my animal to DB
				dbUtil.setUp();
				dbUtil.startTransaction();
				dbUtil.saveProgramare(myProgramare);
				dbUtil.commitTransaction();
				dbUtil.printAllProgramareFromDB();
				dbUtil.closeEntityManager();

	}

public static void updateProgramare() throws Exception {
	

	String s = new String();

		
		System.out.println("Update the Programare with id: ");
		Scanner scanner = new Scanner(System.in);
		int lookForThis = Integer.parseInt(scanner.nextLine());
		
		System.out.println("The IdAnimal for Programare  is :");
		int newIdAnimal = Integer.parseInt(scanner.nextLine());
		
		System.out.println("The Personal Medical for Programare is:");
		int newIdPersonalMedical = Integer.parseInt(scanner.nextLine());
		
		System.out.println("The new date and time for Programare is");
		s = scanner.nextLine();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MM yyyy HH:mm");
	    LocalDateTime date = LocalDateTime.parse(s, formatter);
		
		Date date2 = Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
		
		dbUtil.setUp();
		dbUtil.startTransaction();
		Programare myProgramare = dbUtil.findProgramare(lookForThis);
		myProgramare.setData(date2);
		myProgramare.setIdAnimal(newIdAnimal);
		myProgramare.setIdPersonalMedical(newIdPersonalMedical);
		dbUtil.commitTransaction();
		dbUtil.printAllProgramareFromDB();
		dbUtil.closeEntityManager();
		

	}

public static void deleteProgramare() throws Exception {
	
	System.out.println("Delete the Programare with id: ");
	Scanner scanner = new Scanner(System.in);
	int lookForThis = Integer.parseInt(scanner.nextLine());
	
	dbUtil.setUp();
	dbUtil.startTransaction();
	Programare myProgramare = dbUtil.findProgramare(lookForThis);
	dbUtil.entityManager.remove(myProgramare);
	dbUtil.commitTransaction();
	dbUtil.printAllProgramareFromDB();
	dbUtil.closeEntityManager();
	

}
}
