package util;

import java.util.List;

import java.util.Scanner;

import model.Animal;
import model.Personalmedical;;

public class PersonalMedicalUtil {
	
	static DatabaseUtil dbUtil =new DatabaseUtil();
	
	public static void createPersonalMedical() throws Exception {
		String nume = null;
		int id;
		//Reading doctor's name and id from keyboard
		Scanner scanner=new Scanner(System.in);
		System.out.println("Insert personal medical name");
			nume = scanner.nextLine();
		System.out.println("Insert personal medical id");
			id = Integer.parseInt(scanner.nextLine());
		System.out.println("Insert personal medical specialisation");
			String specialisation = scanner.nextLine();
		
		//throws exception if key is already taken
		//if(idCheck(id)) {
		//	new Exception("The id is already taken, try again ");  //TO DO: do something to show exception message
		//}
			
		
		//creating Personal medical
		Personalmedical myPersonalMedical = new Personalmedical();
		myPersonalMedical.setIdPersonalMedical(id);
		myPersonalMedical.setNume(nume);
		myPersonalMedical.setSpecializare(specialisation);
		
		//pushing my personal medical to DB
		//inserting creating the animal
				
				//pushing my animal to DB
				dbUtil.setUp();
				dbUtil.startTransaction();
				dbUtil.savePersonalMedical(myPersonalMedical);
				dbUtil.commitTransaction();
				dbUtil.printAllPersonalMedicalFromDB();
				dbUtil.closeEntityManager();
	}
	
	


	
	//Method for updating an personal medical after looking by id
	public static void updatePersonalMedical() throws Exception {
		
		System.out.println("Update the Personal Medical with id: ");
		Scanner scanner = new Scanner(System.in);
		int lookForThis = Integer.parseInt(scanner.nextLine());
		
		System.out.println("The new name for the PersonalMedical is :");
		String newName = scanner.nextLine();
		
		System.out.println("The new specialisation for the PersonalMedical is:");
		String newSpecialisation = scanner.nextLine();
		
		dbUtil.setUp();
		dbUtil.startTransaction();
		Personalmedical myPersonalMedical = dbUtil.findPersonalMedical(lookForThis);
		myPersonalMedical.setNume(newName);
		myPersonalMedical.setSpecializare(newSpecialisation);
		dbUtil.commitTransaction();
		dbUtil.printAllPersonalMedicalFromDB();
		dbUtil.closeEntityManager();
		

	}
	
	
	//method for reading all animals
	public static void readPersonalMedical( ) throws Exception{
		
		dbUtil.setUp();
		dbUtil.startTransaction();
		dbUtil.printAllPersonalMedicalFromDB();
		dbUtil.closeEntityManager();
	}
public static void deletePersonalMedical() throws Exception {
		
		System.out.println("Delete the personalMedical with id: ");
		Scanner scanner = new Scanner(System.in);
		int lookForThis = Integer.parseInt(scanner.nextLine());
		
		dbUtil.setUp();
		dbUtil.startTransaction();
		Personalmedical myPersonalMedical = dbUtil.findPersonalMedical(lookForThis);
		dbUtil.entityManager.remove(myPersonalMedical);
		dbUtil.commitTransaction();
		dbUtil.printAllPersonalMedicalFromDB();
		dbUtil.closeEntityManager();
		

	}

public static Personalmedical findPersonalMedical() throws Exception {
	Scanner scanner = new Scanner(System.in);
	int lookForThis = Integer.parseInt(scanner.nextLine());
	
	dbUtil.setUp();
	dbUtil.startTransaction();
	Personalmedical personalMedical =dbUtil.findPersonalMedical(lookForThis);
	dbUtil.closeEntityManager();
	
	return personalMedical;
	
}


}
