package unitTesting;

import static org.junit.jupiter.api.Assertions.*;
import controllers.MainController;
import javafx.collections.ObservableList;

import org.junit.jupiter.api.Test;

class MainControllerTests {

	@Test
	void testGetAvailableSpecies() {
		MainController testController = new MainController();
		ObservableList<String> test = testController.getAvailableSpecies();
		assertEquals("Caine", test.get(0));
	}

}
