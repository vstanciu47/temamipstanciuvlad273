package listener;

public interface OnCompleteListener<T> {
	    T onComplete();
}
