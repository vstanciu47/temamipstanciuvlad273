package listener;

import java.util.List;

import controllers.MainController;
import javafx.collections.ObservableList;
import model.Animal;
import util.DatabaseUtil;

public class LongRunningTask implements Runnable {
	
	private OnCompleteListener onCompleteListener;
	
	 public void setOnCompleteListener(OnCompleteListener onCompleteListener) {
		this.onCompleteListener = onCompleteListener;
	}
	 
	@Override
	public void run() {
		try {
			Thread.sleep(1*1000);
			onCompleteListener.onComplete();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
	}

}
