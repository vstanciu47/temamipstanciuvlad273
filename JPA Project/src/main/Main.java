package main;

import java.io.IOException;
import java.net.ServerSocket;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import listener.LongRunningTask;
import listener.OnCompleteListener;
import server.Server;

public class Main extends Application{

	@Override
	public void start(Stage primaryStage) throws Exception {
		try {
			
			BorderPane root = (BorderPane) FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
			Scene scene = new Scene(root, 800,800);
			primaryStage.setScene(scene);
			primaryStage.show();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
            ServerSocket serverSocket = new ServerSocket(5000);
            while (true) {
                new Server(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
		
	}
	public static void main (String[] args) {
		 OnCompleteListener<Void> test= () -> {
			System.out.println("tready stuff ends");
			return null;
		};
		
		LongRunningTask longRunningTask = new LongRunningTask();
        longRunningTask.setOnCompleteListener(test);
        
        System.out.println("Starting the long running task.");
        longRunningTask.run();
		launch(args);
		
	}
	
	
}