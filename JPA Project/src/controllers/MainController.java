package controllers;

import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import listener.LongRunningTask;
import listener.OnCompleteListener;
import model.Animal;
import model.Personalmedical;
import model.Programare;
import util.DatabaseUtil;

public class MainController implements Initializable {
	

	@FXML
	private ListView<String> animalListView;
	@FXML
	private ListView<String> personalMedicalListView;
	@FXML
	private ListView<String> programareListView;

	//CRUD animal FXML things
	@FXML
	private ChoiceBox<String> animalSpecieChoiceBox ;
	@FXML
	private TextField animalName;
	@FXML
	private TextField createAnimalId;
	
	//CRUD Personal medical FXML things
	@FXML
	private TextField personalMedicalNume;
	@FXML
	private ChoiceBox<String> personalMedicalSpecializare;
	@FXML
	private TextField personalMedicalId;
	
	OnCompleteListener<Void> animalRefresher  = () -> {
		populateAnimalListView();
		return null;
	};
	
	OnCompleteListener<Void> personalMedicalRefresher = () -> {
		populatePersonalMedicalListView();
		return null;
	};
	
	OnCompleteListener<Void> programareRefresher = () -> {
		populateProgramareListView();
		return null;
	};
	
	
	//Populates the animal panel
	public void populateAnimalListView() {
		DatabaseUtil.getInstance().setUp();
		DatabaseUtil.getInstance().startTransaction();
		List<Animal> animalDBList = (List <Animal>) DatabaseUtil.getInstance().animalList();
		ObservableList<String> animalList = getAllAnimals(animalDBList);
		animalSpecieChoiceBox.setItems(getAvailableSpecies());
		animalListView.setItems(animalList);
		animalListView.refresh();
		DatabaseUtil.getInstance().closeEntityManager();
	}
	//Populates the personal medical panel
	public void populatePersonalMedicalListView() {
		DatabaseUtil.getInstance().setUp();
		DatabaseUtil.getInstance().startTransaction();
		personalMedicalSpecializare.setItems(getAvailableSpecies());
		List<Personalmedical> personalMedicalDBList = (List <Personalmedical>) 
				DatabaseUtil.getInstance().personalMedicalList();
		ObservableList<String> personalMedicalList = getAllPersonalMedical(personalMedicalDBList);
		personalMedicalListView.setItems(personalMedicalList);
		personalMedicalListView.refresh();

		DatabaseUtil.getInstance().closeEntityManager();
	}
	
	// Populates programare panel
	private void populateProgramareListView() {
		DatabaseUtil.getInstance().setUp();
		DatabaseUtil.getInstance().startTransaction();
		List<Programare> programareDBList = (List <Programare>) 
				DatabaseUtil.getInstance().programareList();
		ObservableList<String> programareList = getAllProgramare(programareDBList);
		programareListView.setItems(programareList);
		programareListView.refresh(); 
		
		DatabaseUtil.getInstance().closeEntityManager();
		
	}
	//creates an animal when button is pushed
	
	
	//creates an animal
	@FXML
	private void createAnimal() {
		LongRunningTask populate = new LongRunningTask();
		populate.setOnCompleteListener(animalRefresher);
		Animal myAnimal = new Animal(
				animalName.getText(),animalSpecieChoiceBox.getValue(),Integer.parseInt(createAnimalId.getText()));
		DatabaseUtil.getInstance().setUp();
		DatabaseUtil.getInstance().startTransaction();
		DatabaseUtil.getInstance().saveAnimal(myAnimal);
		DatabaseUtil.getInstance().commitTransaction();
		DatabaseUtil.getInstance().closeEntityManager();
		populate.run();
	}
	
	//updates an animal
	@FXML
	private void updateAnimal() {
		LongRunningTask populate = new LongRunningTask();
		populate.setOnCompleteListener(animalRefresher);
		Animal myAnimal = new Animal();
		DatabaseUtil.getInstance().setUp();
		DatabaseUtil.getInstance().startTransaction();
		myAnimal = DatabaseUtil.getInstance().findAnimal(Integer.parseInt(createAnimalId.getText()));
		myAnimal.setSpecie(animalSpecieChoiceBox.getValue());
		myAnimal.setNume(animalName.getText());
		DatabaseUtil.getInstance().commitTransaction();
		DatabaseUtil.getInstance().printAllAnimalsFromDB();
		DatabaseUtil.getInstance().closeEntityManager();
		populate.run();
		
	}
	
	//deletes an animal
	@FXML
	private void deleteAnimal() {
		LongRunningTask populate = new LongRunningTask();
		populate.setOnCompleteListener(animalRefresher);
		DatabaseUtil.getInstance().setUp();
		DatabaseUtil.getInstance().startTransaction();
		Animal myAnimal = DatabaseUtil.getInstance().findAnimal(Integer.parseInt(createAnimalId.getText()));
		DatabaseUtil.getInstance().entityManager.remove(myAnimal);
		DatabaseUtil.getInstance().commitTransaction();
		DatabaseUtil.getInstance().printAllAnimalsFromDB();
		DatabaseUtil.getInstance().closeEntityManager();
		populate.run();
		
	}
	
	//creates personal medical
	@FXML
	private void createPersonalMedical() {
		LongRunningTask populate = new LongRunningTask();
		populate.setOnCompleteListener(personalMedicalRefresher);
		Personalmedical myPersonalMedical = new Personalmedical();
		myPersonalMedical.setNume(personalMedicalNume.getText());
		myPersonalMedical.setSpecializare(personalMedicalSpecializare.getValue());
		myPersonalMedical.setIdPersonalMedical(Integer.parseInt(personalMedicalId.getText()));
		DatabaseUtil.getInstance().setUp();
		DatabaseUtil.getInstance().startTransaction();
		DatabaseUtil.getInstance().savePersonalMedical(myPersonalMedical);
		DatabaseUtil.getInstance().commitTransaction();
		DatabaseUtil.getInstance().printAllAnimalsFromDB();
		DatabaseUtil.getInstance().closeEntityManager();
		populate.run();
		
	}
	//updates personal medical found by id
	@FXML
	private void updatePersonalMedical() {
		LongRunningTask populate = new LongRunningTask();
		populate.setOnCompleteListener(personalMedicalRefresher);
		DatabaseUtil.getInstance().setUp();
		DatabaseUtil.getInstance().startTransaction();
		Personalmedical myPersonalMedical = DatabaseUtil.getInstance().findPersonalMedical
				(Integer.parseInt(personalMedicalId.getText()));
		myPersonalMedical.setNume(personalMedicalNume.getText());
		myPersonalMedical.setSpecializare(personalMedicalSpecializare.getValue());
		DatabaseUtil.getInstance().savePersonalMedical(myPersonalMedical);
		DatabaseUtil.getInstance().commitTransaction();
		DatabaseUtil.getInstance().printAllAnimalsFromDB();
		DatabaseUtil.getInstance().closeEntityManager();
		populate.run();
		
	}
	
	//deletes a personal medical found by id
	@FXML
	private void deletePersonalMedical() {
		LongRunningTask populate = new LongRunningTask();
		populate.setOnCompleteListener(personalMedicalRefresher);
		DatabaseUtil.getInstance().setUp();
		DatabaseUtil.getInstance().startTransaction();
		Personalmedical myPersonalMedical = DatabaseUtil.getInstance().findPersonalMedical(Integer.parseInt(personalMedicalId.getText()));
		DatabaseUtil.getInstance().entityManager.remove(myPersonalMedical);
		DatabaseUtil.getInstance().commitTransaction();
		DatabaseUtil.getInstance().printAllAnimalsFromDB();
		DatabaseUtil.getInstance().closeEntityManager();
		populate.run();
	}
	
	@FXML
	private void createProgramare() {
		
	}
	
	

	//getter for all animals species available
	public ObservableList<String> getAvailableSpecies (){
		ObservableList<String> species = FXCollections.observableArrayList("Caine","Pisica","Hamster","Cal");
		return species;
	}

	public ObservableList<String> getAllAnimals(List<Animal> animals) {
		ObservableList<String> myAnimals = FXCollections.observableArrayList();
		for(Animal a : animals) {
			myAnimals.add(a.getIdAnimal() + " | " + a.getNume() + " | " + a.getSpecie());
		}
		return myAnimals;
		}

	//getter for all animal names
	public ObservableList<String> getAnimalName (List<Animal> animals){
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Animal a: animals) {
			names.add(a.getNume());
		}
		return names;
	}
	//getter for all animal id
	public ObservableList<Integer> getAnimalId (List<Animal> animals){
		ObservableList<Integer> id = FXCollections.observableArrayList();
		for(Animal a: animals) {
			id.add(a.getIdAnimal());
		}
		return id;
	}
	//getter for all animal specie
	public ObservableList<String> getAnimalSpecie(List<Animal> animals){
		ObservableList<String> specie = FXCollections.observableArrayList();
		for(Animal a: animals) {
			specie.add(a.getSpecie());
		}
		return specie;
	}
	
	//getter for all personal medical data to string
	public ObservableList<String> getAllPersonalMedical(List<Personalmedical> personalMedicalList){
		ObservableList<String> personalMedical = FXCollections.observableArrayList();
		for(Personalmedical p : personalMedicalList) {
			personalMedical.add(p.getIdPersonalMedical() + "|"+ p.getNume() + "|" + p.getSpecializare());
		}
		return personalMedical;
	}
	//getter for all personal medical name
	public ObservableList<String> getPersonalMedicalNume(List<Personalmedical> personalMedicalList){
		ObservableList<String> nume = FXCollections.observableArrayList();
		for(Personalmedical p : personalMedicalList){
			nume.add(p.getNume());
		}
		return nume;
	}
	//getter for all personal medical specializare
	public ObservableList<String> getPersonalMedicalSpecializare(List<Personalmedical> personalMedicalList){
		ObservableList<String> specializare = FXCollections.observableArrayList();
		for(Personalmedical p : personalMedicalList){
			specializare.add(p.getSpecializare());
		}
		return specializare;
	}
	//getter for all personal medical id
	public ObservableList<Integer> getPersonalMedicalId (List<Personalmedical> personalMedicalList){
		ObservableList<Integer> id = FXCollections.observableArrayList();
		for(Personalmedical p : personalMedicalList) {
			id.add(p.getIdPersonalMedical());
		}
		return id;
	}
	//getter for all programare converted to string
	public ObservableList<String> getAllProgramare (List<Programare> programareList){
		ObservableList<String> myProgramare = FXCollections.observableArrayList();
		for(Programare p : programareList) {
			myProgramare.add(p.getIdProgramare()+" | " + p.getData() + " | " + p.getIdAnimal() + " | " + p.getIdPersonalMedical());
		}
		return myProgramare;
	}
	
		//getter for all programare date
		public ObservableList<Date> getProgramareDate (List<Programare> programareList){
			ObservableList<Date> date = FXCollections.observableArrayList();
			for(Programare p : programareList) {
				date.add(p.getData());
			}
			return date;
		}
	
	//getter for all programare id 
	public ObservableList<Integer> getProgramareId(List<Programare> programareList){
		ObservableList<Integer> id = FXCollections.observableArrayList();
		for(Programare p : programareList) {
			id.add(p.getIdAnimal());
		}
		return id;
	}
	
	//getter for all programare animal id
	public ObservableList<Integer> getProgramareAnimalId(List<Programare> programareList){
		ObservableList<Integer> id = FXCollections.observableArrayList();
		for(Programare p : programareList) {
			id.add(p.getIdAnimal());
		}
		return id;
	}
	
	//getter for all programare personal medical id
	public ObservableList<Integer> getProgramarePersonalMedicalId(List<Programare> programareList){
		ObservableList<Integer> id = FXCollections.observableArrayList();
		for(Programare p : programareList) {
			id.add(p.getIdPersonalMedical());
		}
		return id;
	}


	@Override
	public void initialize(URL location, ResourceBundle resources) {
		populateAnimalListView();
		populatePersonalMedicalListView();
		populateProgramareListView();
	}

}
